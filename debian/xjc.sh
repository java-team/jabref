#!/bin/sh
#
# generate Java bindings for XML schemas used in JabRef

XJC="java -cp /usr/share/java/jaxb-xjc.jar:/usr/share/java/jaxb-runtime.jar:/usr/share/java/xml-resolver.jar:/usr/share/java/relaxngDatatype.jar com.sun.tools.xjc.XJCFacade"
DEST=src/main/gen

$XJC -d $DEST \
	-p net.sf.jabref.logic.importer.fileformat.medline \
	src/main/resources/xjc/medline/medline.xsd

$XJC -d $DEST \
	-p net.sf.jabref.logic.importer.fileformat.bibtexml \
	src/main/resources/xjc/bibtexml/bibtexml.xsd

$XJC -d $DEST -npa -nv -catalog src/main/resources/xjc/mods/catalog.cat \
	-b src/main/resources/xjc/mods/mods-binding.xjb \
	-p net.sf.jabref.logic.importer.fileformat.mods \
	src/main/resources/xjc/mods/mods-3-6.xsd
