#!/bin/sh

# Include the wrappers utility script
. /usr/lib/java-wrappers/java-wrappers.sh

# We need a java8 runtime (at least)
find_java_runtime java8

MAJOR_JAVA_VERSION=$(run_java -version 2>&1 | grep ' version ' | awk '{print $3}' | cut -f1 -d'.' | cut -c2-)
if ( test $MAJOR_JAVA_VERSION -ge 9 ) ; then
    # We need some options to start this version on openjdk9 and later
    # See http://discourse.jabref.org/t/cannot-start-jabref-3-7-3-6-using-java-9-on-ubuntu-16-04/361/8
    JABREF_JAVA_OPTS=${JABREF_JAVA_OPTS-"--add-opens=java.desktop/java.awt=ALL-UNNAMED"}

    # But java.se.ee only works for Java 9 and Java 10
    if ( test $MAJOR_JAVA_VERSION -lt 11 ) ; then
        JABREF_JAVA_OPTS="${JABREF_JAVA_OPTS} --add-modules=java.se.ee"
    fi
fi

find_jars \
	jabref \
	bcprov \
	antlr3-runtime \
	antlr4-runtime \
	com.android.json \
	commons-cli \
	commons-codec \
	commons-lang3 \
	commons-logging \
	fontbox \
	glazedlists \
	guava \
	httpasyncclient \
	httpclient \
	httpcore \
	httpcore-nio \
	httpmime \
	java-string-similarity \
	jaxb-runtime \
	jempbox \
	jgoodies-common \
	jgoodies-forms \
	jgoodies-looks \
	jhlabs-filters \
	jsoup \
	juh \
	jurt \
	log4j-api \
	log4j-core \
	log4j-jcl \
	microba \
	mariadb-java-client \
	pdfbox \
	postgresql \
	ridl \
	spin \
	swingx \
	swing-layout \
	unirest-java \
	unoil

run_java ${JABREF_JAVA_OPTS} net.sf.jabref.JabRefMain "$@"
